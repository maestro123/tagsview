package maestro.tagsview;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom.Borovsky on 12.10.2015.
 */
public class TagsView extends ViewGroup implements View.OnClickListener {

    public static final String TAG = TagsView.class.getSimpleName();

    private ArrayList<Tag> mTags = new ArrayList<Tag>();
    private SparseArray<ArrayList<View>> mViewMap = new SparseArray<ArrayList<View>>();
    private OnTagClickListener mClickListener;

    private int mPadding;
    private int mBackgroundResource;
    private int mTextAppearance;
    private int mTagInnerPadding;

    private boolean isReorderRequired;
    private boolean isRequestAllow;

    public interface OnTagClickListener {
        void onTagClick(Tag tag);
    }

    public TagsView(Context context) {
        super(context);
        init(null);
    }

    public TagsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public TagsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private final void init(AttributeSet attrs) {
        mPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources().getDisplayMetrics());
        if (attrs != null) {
            TypedArray a = getContext().getTheme().obtainStyledAttributes(attrs, R.styleable.TagsView, 0, 0);
            mBackgroundResource = a.getResourceId(R.styleable.TagsView_tagBackground, -1);
            mTextAppearance = a.getResourceId(R.styleable.TagsView_tagTextAppearance, -1);
            mTagInnerPadding = a.getDimensionPixelSize(R.styleable.TagsView_tagInnerPadding, mPadding);
            Log.e(TAG, "mBackgroundResource: " + mBackgroundResource);
        }
        setPadding(mPadding, mPadding, mPadding, mPadding);
    }

    //TODO: check onConfigurationChanged and onSizeChanged for reordering, if not then call requestLayout();

    @Override
    protected void onConfigurationChanged(Configuration newConfig) {
        isReorderRequired = true;
        super.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        isReorderRequired = true;
        super.onSizeChanged(w, h, oldw, oldh);
    }

    @Override
    public void requestLayout() {
        if (isRequestAllow) {
            super.requestLayout();
        }
    }

    @Override
    public void onClick(View v) {
        if (mClickListener != null) {
            final Tag tag = (Tag) v.getTag();
            if (tag != null) {
                mClickListener.onTagClick(tag);
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int widthSpec = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);
        final int heightSpec = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        final int widthMode = MeasureSpec.getMode(widthMeasureSpec);
        final int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        final int widthMax = MeasureSpec.getSize(widthMeasureSpec);
        final int heightMax = MeasureSpec.getSize(heightMeasureSpec);
        final int childCount = getChildCount();

        measureChildren(widthMeasureSpec, heightMeasureSpec);

        int adjustWidth = 0;
        int adjustHeight = 0;

        if (isReorderRequired) {

            final int maxWidth = widthMax - getPaddingLeft() - getPaddingRight();

            int currentRow = 0;

            mViewMap.clear();
            mViewMap.put(currentRow, new ArrayList<View>());

            int[] rowsWidth = new int[1];

            for (int i = 0; i < childCount; i++) {

                final View child = getChildAt(i);
                final int childWidth = child.getMeasuredWidth();
                final int childHeight = child.getMeasuredHeight();

                sort:
                {
                    for (int j = 0; j < rowsWidth.length; j++) {
                        if (rowsWidth[j] + childWidth + mPadding < maxWidth) {
                            rowsWidth[j] += childWidth + mPadding;
                            mViewMap.get(j).add(child);
                            adjustWidth = Math.max(adjustWidth, rowsWidth[j]);
                            if (i == 0) {
                                adjustHeight += getPaddingTop() + childHeight + mPadding;
                            }
                            break sort;
                        }
                    }

                    int[] nRowsWidth = new int[rowsWidth.length + 1];
                    System.arraycopy(rowsWidth, 0, nRowsWidth, 0, rowsWidth.length);
                    rowsWidth = nRowsWidth;
                    currentRow++;
                    mViewMap.put(currentRow, new ArrayList<View>());
                    adjustHeight += childHeight + mPadding;
                    rowsWidth[currentRow] += childWidth + mPadding;
                    mViewMap.get(currentRow).add(child);
                    adjustWidth = Math.max(adjustWidth, Math.min(childWidth, maxWidth));
                }
            }

        }

        if (widthMeasureSpec == MeasureSpec.EXACTLY && heightMeasureSpec == MeasureSpec.EXACTLY) {
            setMeasuredDimension(widthSpec, heightSpec);
            return;
        }

        int width, height;

        if (MeasureSpec.EXACTLY == widthMode) {
            width = widthSpec;
        } else if (MeasureSpec.AT_MOST == widthMode) {
            width = Math.min(widthMax, adjustWidth);
        } else {
            width = adjustWidth;
        }

        if (MeasureSpec.EXACTLY == heightMode) {
            height = heightSpec;
        } else if (MeasureSpec.AT_MOST == heightMode) {
            height = Math.min(heightMax, adjustHeight);
        } else {
            height = adjustHeight;
        }

        setMeasuredDimension(width, height);
    }

    @Override
    protected void onLayout(boolean changed, int l, int t, int r, int b) {
        final int childCount = getChildCount();
        if (childCount == 0 || mViewMap.size() == 0) return;

        final int width = getWidth() - getPaddingLeft() - getPaddingRight();

        int left = getPaddingLeft();
        int top = getPaddingTop();

        for (int i = 0; i < mViewMap.size(); i++) {
            for (int j = 0; j < mViewMap.get(i).size(); j++) {
                final View v = mViewMap.get(i).get(j);
                final int childWidth = v.getMeasuredWidth();
                final int childHeight = v.getMeasuredHeight();
                v.layout(left, top, Math.min(width, left + childWidth), top + childHeight);
                left += childWidth + mPadding;
                if (j == mViewMap.get(i).size() - 1) {
                    top += childHeight + mPadding;
                }
            }
            left = getPaddingLeft();
        }

    }

    private final View makeTagView(Tag tag) {
        TextView textView = new TextView(getContext());
        textView.setText(tag.text);
        textView.setSingleLine();
        textView.setTextSize(14);
        textView.setPadding(mTagInnerPadding, mTagInnerPadding / 4, mTagInnerPadding, mTagInnerPadding / 4);
        if (mBackgroundResource != -1) {
            textView.setBackgroundResource(mBackgroundResource);
        }
        if (mTextAppearance != -1) {
            textView.setTextAppearance(getContext(), mTextAppearance);
        }
        textView.setTag(tag);
        textView.setOnClickListener(this);
        return textView;
    }

    public void addTags(int arrayId) {
        addTags(getResources().getStringArray(arrayId));
    }

    public void addTags(List<String> tags) {
        if (tags != null && tags.size() > 0) {
            isRequestAllow = false;
            for (String str : tags) {
                addTag(str);
            }
            isRequestAllow = true;
            requestLayout();
        }
    }

    public void addTags(String[] tags) {
        if (tags != null && tags.length > 0) {
            isRequestAllow = false;
            for (String str : tags) {
                addTag(str);
            }
            isRequestAllow = true;
            requestLayout();
        }
    }

    public void addTags(Tag[] tags) {
        if (tags != null && tags.length > 0) {
            isRequestAllow = false;
            for (Tag tag : tags) {
                addTag(tag);
            }
            isRequestAllow = true;
            requestLayout();
        }
    }

    public void addTag(String text) {
        addTag(new Tag(text));
    }

    public void addTag(Tag tag) {
        if (!mTags.contains(tag)) {
            isReorderRequired = true;
            addView(makeTagView(tag));
        }
    }

    public void setOnTagClickListener(OnTagClickListener listener) {
        mClickListener = listener;
    }

    public static final class Tag {

        private String text;
        private Object object;

        public Tag() {
        }

        public Tag(String text) {
            this.text = text;
        }

        public Tag(String text, Object object) {
            this.text = text;
            this.object = object;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public Object getObject() {
            return object;
        }

        public void setObject(Object object) {
            this.object = object;
        }

        @Override
        public boolean equals(Object o) {
            if (o == this) {
                return true;
            } else if (o instanceof Tag && !TextUtils.isEmpty(((Tag) o).text)) {
                return ((Tag) o).text.equals(text);
            }
            return super.equals(o);
        }
    }

}
