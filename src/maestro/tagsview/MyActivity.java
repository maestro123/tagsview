package maestro.tagsview;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyActivity extends Activity {

    private TagsView mTagsView;

    public static final List<String> DATA = Arrays.asList("android", "library", "collection",
            "hashtags", "min14sdk", "UI", "view", "github", "opensource", "project", "widget");

    public static final List<String> TAGS = Arrays.asList("cupcake", "donut", "eclair", "froyo",
            "gingerbread", "honeycomb", "icecreamsandwich", "jellybean", "kitkat", "lollipop", "marshmallow");

    public static final List<String> PEOPLE = Arrays.asList("wolverine", "jubilee", "colossus",
            "beast", "rogue", "storm", "cyclops", "iceman", "magma", "emmafrost", "angel");

    public static final List<String> ALL = new ArrayList<String>();

    static {
        ALL.addAll(DATA);
        ALL.addAll(TAGS);
        ALL.addAll(PEOPLE);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        mTagsView = (TagsView) findViewById(R.id.tags_view);
        mTagsView.addTags(ALL);
        mTagsView.setOnTagClickListener(new TagsView.OnTagClickListener() {
            @Override
            public void onTagClick(TagsView.Tag tag) {
                Toast.makeText(getApplicationContext(), tag.getText(), Toast.LENGTH_SHORT).show();
            }
        });

    }
}
